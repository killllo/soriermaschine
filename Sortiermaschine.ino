# define EN 8 // stepper motor enable , active low
# define ENZ 1 // stepper motor enable , active low
# define X_DIR 5 // X -axis stepper motor direction control
# define Y_DIR 6 // y -axis stepper motor direction control
# define Z_DIR 7 // z axis stepper motor direction control
# define A_DIR 12 // A axis stepper motor direction control
# define X_STP 2 // x -axis stepper control
# define Y_STP 3 // y -axis stepper control
# define Z_STP 4 // z -axis stepper control
# define A_STP 13 // z -axis stepper control
# define LED 48
# define SENSOR_X 50
# define SENSOR_Y 51
#define S0 32
#define S1 34
#define S2 36
#define S3 38
#define sensorOut 40


//// *
/// / Function : step . function: to control the direction of the stepper motor , the number of steps .
/// / Parameters : dir direction control , dirPin corresponding stepper motor DIR pin , stepperPin corresponding stepper motor " step " pin , Step number of step of no return value.
////
int N = 1;
int delayUs = 500;
int delayUs2 = 3500;
int steps = 100;
int color_bin = 0;
int current_bin;
int R_frequency = 0, G_frequency = 0, B_frequency = 0;
int col_R = 0, col_G = 0, col_B = 0;
int hole = 0, count=0;
int unknowns =0;
float RG_Ratio = 0, RB_Ratio=0, GB_Ratio=0;
float Rs=0, Gs=0, Bs=0;
float RGBmin=0, RGBmax=0;
float Hue=0, Sat=0, Val=0;


void step (boolean dir, byte dirPin, byte stepperPin, int steps)
{
 digitalWrite (dirPin, dir);
 delay (50);
 for (int i = 0; i < steps; i ++)
 {
   digitalWrite (stepperPin, HIGH);
   delayMicroseconds (delayUs);
   digitalWrite (stepperPin, LOW);
   delayMicroseconds (delayUs);
 }
}
void step2 (boolean dir, byte dirPin, byte stepperPin, int steps)
{
 digitalWrite (dirPin, dir);
 delay (50);
 for (int i = 0; i < steps; i ++)
 {
   digitalWrite (stepperPin, HIGH);
   delayMicroseconds (delayUs2);
   digitalWrite (stepperPin, LOW);
   delayMicroseconds (delayUs2);
 }
}
void detect_color (){
  digitalWrite(S0,HIGH);  // Setting frequency-scaling to 20%
  digitalWrite(S1,LOW);  // Setting frequency-scaling to 20%
  digitalWrite(LED, HIGH); // Turn the LED on
  delay(100);

  Serial.print("Perle #: ");
  Serial.print(count);
  Serial.print("  ");
  Serial.print("Unknown perls : ");
  Serial.print(unknowns);
  Serial.print("  ");
  Serial.print("Hole: ");
  Serial.print(hole);
  Serial.print("  ");
  
  count++;
  
  R_frequency = 0;
  G_frequency = 0;
  B_frequency = 0;
  
  for (int i=1; i<2; i ++)  {
    // Setting red filtered photodiodes to be read
    digitalWrite(S2,LOW);
    digitalWrite(S3,LOW);
delay(20);
    // Reading the output frequency
    R_frequency = pulseIn(sensorOut, LOW) + R_frequency;
  
    // Printing the value on the serial monitor
    //Serial.print("R= ");//printing name

  
    // Setting Green filtered photodiodes to be read
    digitalWrite(S2,HIGH);
    digitalWrite(S3,HIGH);
delay(20);
    // Reading the output frequency
    G_frequency = pulseIn(sensorOut, LOW) + G_frequency;
    // Printing the value on the serial monitor
    //Serial.print("G= ");//printing name

  
    // Setting Blue filtered photodiodes to be read
    digitalWrite(S2,LOW);
    digitalWrite(S3,HIGH);
delay(20);
    // Reading the output frequency
    B_frequency = pulseIn(sensorOut, LOW) + B_frequency;  

  }

 // R_frequency = round(0.25*R_frequency);
 // G_frequency = round(0.25*G_frequency);
 // B_frequency = round(0.25*B_frequency);
  
  Serial.print("Frequency: ");
  Serial.print(R_frequency);//printing RED color frequency
  Serial.print(",");
  Serial.print(G_frequency);//printing RED color frequency
  Serial.print(",");
  Serial.print(B_frequency);//printing RED color frequency
  Serial.print(",");
  Serial.print("--");
  Serial.print(",");
  
  Serial.print(" , ");
  switch (hole) {
    case 1: col_R = map(R_frequency,300,2232,255,0);
            col_G = map(G_frequency,290,2252,255,0);
            col_B = map(B_frequency,220,1687,255,0);
            break;
    case 2: col_R = map(R_frequency,300,2103,255,0);
            col_G = map(G_frequency,290,2137,255,0);
            col_B = map(B_frequency,220,1593,255,0);
            break;
    case 3: col_R = map(R_frequency,300,1938,255,0);
            col_G = map(G_frequency,290,1960,255,0);
            col_B = map(B_frequency,220,1462,255,0);
            break;
    case 4: col_R = map(R_frequency,300,2137,255,0);
            col_G = map(G_frequency,290,2152,255,0);
            col_B = map(B_frequency,220,1604,255,0);
            break;
    case 5: col_R = map(R_frequency,300,2182,255,0);
            col_G = map(G_frequency,290,2192,255,0);
            col_B = map(B_frequency,220,1639,255,0);
            break;
    case 6: col_R = map(R_frequency,300,2310,255,0);
            col_G = map(G_frequency,290,2345,255,0);
            col_B = map(B_frequency,220,1751,255,0);
            break;
    default: break;
  }

            
  Serial.print("RGB: ");
  Serial.print(col_R);//printing RED color mapped value 0 to 255
  Serial.print(",");
  Serial.print(col_G);//printing GREEN color mapped value 0 to 255
  Serial.print(",");
  Serial.print(col_B);//printing BLUE color mapped value 0 to 255
  Serial.print(",");
  Serial.print("  ");
  digitalWrite(LED, LOW); // Turn the LED off

  // This part is orginially from https://www.youtube.com/watch?v=LNxA1oXaZy4, Mack Electronic
  Rs = col_R/255.0; Gs = col_G/255.0; Bs = col_B/255.0; 
    Serial.print("Stuff1: "); 
  Serial.print(Rs,3); Serial.print(","); Serial.print(Gs,3); Serial.print(","); Serial.print(Bs,3); Serial.print(",");

  RGBmin = min(Rs, Gs); RGBmin = min(RGBmin, Bs); RGBmax = max(Rs, Gs); RGBmax = max(RGBmax, Bs);
  Serial.print(RGBmin,3); Serial.print(","); Serial.print(RGBmax,3); Serial.print(",");

  if (RGBmax==Rs) {
    Hue = 60.0 * (0+((Gs-Bs)/(RGBmax-RGBmin)) );
  }
  else if (RGBmax==Gs) {
    Hue = 60.0 * (2+((Bs-Rs)/(RGBmax-RGBmin)) );
  }
  else if (RGBmax==Bs) {
    Hue = 60.0 * (4+((Rs-Gs)/(RGBmax-RGBmin)) );
  }
  else if (RGBmax==RGBmin) {
      Hue=0;
  }

  if (RGBmax==0) {
    Sat=0;
  }
  else {
    Sat=(RGBmax-RGBmin)/(RGBmax);
  }
  Val=RGBmax;
  
      Serial.print("Stuff2: ");
  Serial.print(Hue); Serial.print(","); Serial.print(Sat,3); Serial.print(","); Serial.print(Val,3); Serial.print(",");
  
 if ((Hue>-2 && Hue<280 && Sat>0 && Sat<0.03 && Val>0.8 && Val<1.1) || (isnan(Hue) && (RGBmin>200)) ) {
    Serial.print("WHITE");
    Serial.println(",");
    color_bin = 13;    
  } else if  ((Hue>175 && Hue<280 && Sat>-20 && Sat<2 && Val>-20 && Val<0.15) || (col_R+col_G+col_B < 25)) {
    Serial.print("BLACK");
    Serial.println(",");
    color_bin = 12;    
   } else if (Hue>15 && Hue<65 && Sat>0 && Sat<0.05 && Val>0.8 && Val<1.1) {
    Serial.print("Gray");
    Serial.println(",");
    color_bin = 6;  
  } else if  (Hue>-11 && Hue<0 && Sat>0.3 && Sat<0.9 && Val>0.5 && Val<0.95) {
    Serial.print("RED"); 
    color_bin = 9;
    Serial.println(",");
   } else if  (Hue>-1 && Hue<15 && Sat>0 && Sat<0.05 && Val>0.7 && Val<1.1) {
    Serial.print("HAUT"); 
    color_bin = 8;
    Serial.println(",");
  } else if  (Hue>240 && Hue<260 && Sat>0.05 && Sat<0.2 && Val>0.8 && Val<1) {
    Serial.print("VIOLET"); 
    color_bin = 7;
    Serial.println(",");
  } else if (Hue>40 && Hue<53 && Sat>0.2 && Sat<0.7 && Val>0.6 && Val<1.1) {
    Serial.print("YELLOW"); 
    color_bin = 14;
    Serial.println(",");
  } else if (Hue>-42 && Hue<-30 && Sat>0.15 && Sat<0.45 && Val>0.8 && Val<1.1) {
    Serial.print("NEON_PINK"); 
    color_bin = 11;
    Serial.println(",");
  } else if (Hue>195 && Hue<205 && Sat>0.25 && Sat<0.66 && Val>0.8 && Val<1) {
    Serial.print("BLUE"); 
    color_bin = 16;
    Serial.println(",");
  } else if  (Hue>8 && Hue<23 && Sat>0.25 && Sat<0.75 && Val>0.65 && Val<1) {
    Serial.print("ORANGE"); 
    color_bin = 10;
    Serial.println(",");
  } else if (Hue>-1 && Hue<15 && Sat>0.25 && Sat<1.5&& Val>0.15 && Val<0.7) {
    Serial.print("BROWN"); 
    color_bin = 5;
    Serial.println(",");
  } else if (Hue>120 && Hue<135 && Sat>0.2 && Sat<0.91 && Val>0.35 && Val<0.8) {
    Serial.print("GREEN"); 
    color_bin = 15;
    Serial.println(",");
  } else {
    color_bin = 17;
    Serial.print("Unkown");
    Serial.println(",");
    unknowns += 1;
  }
  
  int steps_bin = round(4.44*15*color_bin);
  int old_steps_bin = round(4.44*15*current_bin);
  int diff_steps = steps_bin - old_steps_bin;
  current_bin = color_bin;
  if(diff_steps > 0)  {
    step (true, Y_DIR, Y_STP, diff_steps);
  } else {
    step (false, Y_DIR, Y_STP, abs(diff_steps));
  }

    digitalWrite(S0,LOW);  // Setting frequency-scaling to 20%
  digitalWrite(S1,LOW);  // Setting frequency-scaling to 20%
}

void setup () {   // The stepper motor used in the IO pin is set to output
 pinMode (X_DIR, OUTPUT); 
 pinMode (X_STP, OUTPUT);
 pinMode (Y_DIR, OUTPUT); 
 pinMode (Y_STP, OUTPUT);
 pinMode (Z_DIR, OUTPUT); 
 pinMode (Z_STP, OUTPUT);
 pinMode (A_DIR, OUTPUT); 
 pinMode (A_STP, OUTPUT);
 pinMode (EN, OUTPUT);
 digitalWrite (EN, LOW);
 pinMode (ENZ, OUTPUT);
 digitalWrite (ENZ, LOW);


  Serial.begin(9600);
 
 Serial.println("Starting Sortiermaschine");

  pinMode(S0, OUTPUT);
  pinMode(S1, OUTPUT);
  pinMode(S2, OUTPUT);
  pinMode(S3, OUTPUT);
  pinMode(sensorOut, INPUT);


  pinMode(LED, OUTPUT); // Declare the LED as an output
  pinMode(SENSOR_X, INPUT); // Declare the LED as an output


  
  step (true, Y_DIR, Y_STP, 50);

  int endpoint_y_axis = 0;
  digitalWrite (Y_DIR, LOW);
  delay (50);
  while(!endpoint_y_axis) {
     digitalWrite (Y_STP, HIGH);
     delayMicroseconds (delayUs2);
     digitalWrite (Y_STP, LOW);
     delayMicroseconds (delayUs2);
     endpoint_y_axis = digitalRead(SENSOR_Y);
  }
  current_bin = 0;
  color_bin=17;
  int steps_bin = round(4.44*15*color_bin);
  int old_steps_bin = round(4.44*15*current_bin);
  int diff_steps = steps_bin - old_steps_bin;
  current_bin = color_bin;
  if(diff_steps > 0)  {
    step (true, Y_DIR, Y_STP, diff_steps);
  } else {
    step (false, Y_DIR, Y_STP, abs(diff_steps));
  }
  delay(100);
  
  int endpoint_reached = 0;
  digitalWrite (X_DIR, LOW);
  delay (50);
  while(!endpoint_reached) {
     digitalWrite (X_STP, HIGH);
     delayMicroseconds (delayUs2);
     digitalWrite (X_STP, LOW);
     delayMicroseconds (delayUs2);
     endpoint_reached = digitalRead(SENSOR_X);
  }
  hole = 1;
  delay(150);
}

void loop () {

  hole = 2;
  step2 (true, X_DIR, X_STP, 266);
  delay(100);
  detect_color();

  hole = 3;
  step2 (true, X_DIR, X_STP, 267);
  delay(100);
  detect_color();  

  hole = 4;
  step2 (true, X_DIR, X_STP, 267);
  delay(100);
  detect_color();

  hole = 5;
  step2 (true, X_DIR, X_STP, 267);
  delay(100);
  detect_color();
 
  hole = 6;
  step2 (true, X_DIR, X_STP, 266);
  delay(100);
  detect_color();
 
  hole = 1;
  step2 (true, X_DIR, X_STP, 267);
  delay(100);
  detect_color();
 

}
